import Vue from 'vue'

/** Container */
import Container from './container/Full'

/** Header */
import Header from './container/Header'

/** breadcrumb */
import Breadcrumb from './container/Breadcrumb'

/** Leftside */
import Leftside from './container/Leftside'

/** Footer */
import Footer from './container/Footer'

/** Tree view */
import Treeview from './entity/Treeview'

/** Tree of select  */
import TreeSelect from './entity/TreeSelect'
/** Components root */
//import Dashboard from './dashboard/Dashboard';
import UsersManagement from './users/users'

/** Entity component */
//import Entity from './entity/entity';
/** Vuex store */
import store from './store'




Vue.component('container',Container);
Vue.component('sheader',Header);
Vue.component('sfooter',Footer);
Vue.component('breadcrumbs',Breadcrumb);
Vue.component('leftside',Leftside); 
Vue.component('tree',Treeview);
Vue.component('treeselect',TreeSelect);
Vue.component('users',UsersManagement);
//Vue.component('dashboard',Dashboard);
//Vue.component('entity',Entity);
