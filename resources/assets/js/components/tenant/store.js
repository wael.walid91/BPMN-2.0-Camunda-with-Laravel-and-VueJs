import Vue from 'vue' ;
import Vuex from 'vuex' ;
import network from '../../network'
import languages from '/var/www/html/bpmn-php/resources/assets/js/components/tenant/lang-en.json';


Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        tenant:'',
        baseUrl:"http://localhost:9000", // API BASE URI
        appUrl:"http://localhost:8000/client",
        apiToken:"",
        user:{},
        authGuard:"admin",
        defaultLocale:"en",
        activeLang:'english',
        locales:{
            'en':{
                "key" : "en",
                "name" : "english"
            },
            'ar':{
                "key" : "ar",
                "name" : "العربية"
            }
        },
        languages:languages
    },
    getters: {
        getBaseUrl: state => {
            return state.baseUrl;
        },
        getTanent:state => {
                return state.tanent;
        },
        getlangName:state => {
            return state.locales[state.activeLang].name;
        },
        

    }
})