@extends('tenant.admin.layout.main')
@section('content')

<div class="app flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card-group mb-0">
            <div class="card p-4">
              <div class="card-body">
                <h1>Login</h1>

            <form accept-charset="UTF-8" role="form" method="POST" action="{{ url(\App\Helpers\Api\GenerateUrl::createLink('login')) }}">
                                {{ csrf_field() }}    
                <p class="text-muted">Sign In to your account</p>
                <div class="input-group  mb-3">
                  <span class="input-group-addon"><i class="icon-user"></i></span>
                  <input type="email" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus placeholder="E-mail" name="email" type="text" required>
                  
                </div>

                @if ($errors->has('email'))
                        <span class="help-block {{ $errors->has('email') ? ' has-error' : '' }}">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                  @endif
                <div class="input-group mb-4">
                  <span class="input-group-addon"><i class="icon-lock"></i></span>
                  <input id="password" class="form-control" placeholder="Password" name="password" type="password" required>

                </div>


                @if ($errors->has('password'))
                        <span class="help-block {{ $errors->has('password') ? ' has-error' : '' }}">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                <div class="row">
                  <div class="col-6">
                    <input type="submit" type="button" value="Login" class="btn btn-primary px-4">
                  </div>
                  <div class="col-6 text-right">
                    <a href="{{ url(\App\Helpers\Api\GenerateUrl::createLink('password/reset')) }}"  class="btn btn-link px-0">Forgot password?</a>
                  </div>
                </div>
              </div>
            </div>
        </form>
            <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
              <div class="card-body text-center">
                <div>
                  <h2>Sign up</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                  <button type="button" class="btn btn-primary active mt-3">Register Now!</button>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
  
@endsection

