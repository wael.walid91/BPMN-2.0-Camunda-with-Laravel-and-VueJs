<?php

namespace App\Models\Org;

use Illuminate\Database\Eloquent\Model;

class OrgProducts extends Model
{
    protected $table = "OrgProducts";
    protected $primaryKey = "idOrgProducts"; 
}
