<?php

namespace App\Models\Tenant\Admin;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class TenantAdmin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "TenantAdmin";
    protected $fillable = [
        'name', 'email', 'password' , 'isActive' ,'activationKey', 'Tenants_idTenants' , 'defaultPassword'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
