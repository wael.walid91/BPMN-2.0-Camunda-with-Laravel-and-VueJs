<?php

namespace App\Helpers\Api;

use Illuminate\Http\Request;


class GenerateUrl
{
    public function __construct(){
        $this->middleware('auth');
    }
    public static function createLink($var){
        if(session('active_tanent')){
            return url("client/".session('active_tanent')."/admin/".$var);
        }
        return null ;
    }

    public static function AdminLink($var){
        if(session('active_tanent')){
            return url("client/".session('active_tanent')."/admin/".$var);
        }
        return null ;
    }

    public static function ApiLink($var){
        if(session('active_tanent')){
            return env("ApiBackEnd")."/client/".session('active_tanent')."/".$var;
        }
        return null ;
    }
}
