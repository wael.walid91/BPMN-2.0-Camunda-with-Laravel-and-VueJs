<?php

namespace App\Http\Controllers\Tenant\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class DashboardController extends Controller
{
    public function __construct(){
        $this->middleware('auth_api');
    }
    public function index(){
        $this->middleware('auth_api');
        return View('tenant.admin.home');
    }
}
