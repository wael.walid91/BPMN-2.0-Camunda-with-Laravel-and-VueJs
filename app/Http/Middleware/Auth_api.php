<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Api\GenerateUrl;
use \Session;


class Auth_api
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(!Session::get('user_data')){
			Session::forget('user_data');
			echo "that is logged out" ;
			//return redirect(GenerateUrl::AdminLink("logout"));
		}
		$redisData = json_decode(\Redis::get('user_'.$request->session()->get('user_data.user.id')),true);

		if (
			!$request->session()->get('user_data.user.token') 
			OR (($request->session()->get('user_data.user.token') != $redisData['user']['token']))
			OR !isset($redisData)
			) {
			
			return redirect(GenerateUrl::AdminLink("logout"));
		}
		//dd($request->session()->get('user_data.user.token'));
		$userId = $request->session()->get('user_data.user.id') ;

		/** Sync Redis user data with session */
		$request->session()->put('user_data',json_decode(\Redis::get('user_'.$userId),true));

		//dd(session('user_data.user'));
	    return $next($request);
	}
}