<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('orgadmin')->user();

    //dd($users);

    return view('orgadmin.home');
})->name('home');

