<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'HomeController@index')->name('home');
// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/{vue_capture?}', function () {
//     return view('coreui');
// })->where('vue_capture', '[\/\w\.-]*');

/** Cors miidleware to manage request access */
Route::middleware(['cors'])->group(function () {

    /** Set prefix for Tenant as clients */
    Route::group(['prefix' => 'client/{tenant}' , 'middleware' => ['validTenant']], function () {

        /** Routes for Dashboard controller */
        Route::group(['prefix' => '/'], function () {
          Route::get('/', '\App\Http\Controllers\Tenant\Admin\DashboardController@index')->name('index');
          Route::get('/home', '\App\Http\Controllers\Tenant\Admin\DashboardController@index')->name('home');
          Route::get('/dashboard', '\App\Http\Controllers\Tenant\Admin\DashboardController@index')->name('dashboard');
        });
        Route::group(['prefix' => 'admin'], function () {
          
          /** Routes for Dashboard controller */
        Route::group(['prefix' => '/'], function () {
          Route::get('/', '\App\Http\Controllers\Tenant\Admin\DashboardController@index')->name('index');
          Route::get('/home', '\App\Http\Controllers\Tenant\Admin\DashboardController@index')->name('home');
        });
        
          Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
          Route::post('/login', 'Auth\LoginController@login');
          Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
        
          Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
          Route::post('/register', 'Auth\RegisterController@register');
        
          Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
          Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.email');
          Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
          Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
        });
        
        // Route::group(['prefix' => 'user'], function () {
        //   Route::get('/login', 'UserAuth\LoginController@showLoginForm')->name('login');
        //   Route::post('/login', 'UserAuth\LoginController@login');
        //   Route::post('/logout', 'UserAuth\LoginController@logout')->name('logout');
        
        //   Route::get('/register', 'UserAuth\RegisterController@showRegistrationForm')->name('register');
        //   Route::post('/register', 'UserAuth\RegisterController@register');
        
        //   Route::post('/password/email', 'UserAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
        //   Route::post('/password/reset', 'UserAuth\ResetPasswordController@reset')->name('password.email');
        //   Route::get('/password/reset', 'UserAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
        //   Route::get('/password/reset/{token}', 'UserAuth\ResetPasswordController@showResetForm');
        // });

        // Route::group(['prefix' => 'orgadmin'], function () {
        //   Route::get('/homes', '\App\Http\Controllers\Tenant\Admin\HomeController@index')->name('home');
        //   Route::get('/login', 'OrgAuth\LoginController@showLoginForm')->name('login');
        //   Route::post('/login', 'OrgAuth\LoginController@login');
        //   Route::post('/logout', 'OrgAuth\LoginController@logout')->name('logout');
        
        //   Route::get('/register', 'OrgAuth\RegisterController@showRegistrationForm')->name('register');
        //   Route::post('/register', 'OrgAuth\RegisterController@register');
        
        //   Route::post('/password/email', 'OrgAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
        //   Route::post('/password/reset', 'OrgAuth\ResetPasswordController@reset')->name('password.email');
        //   Route::get('/password/reset', 'OrgAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
        //   Route::get('/password/reset/{token}', 'OrgAuth\ResetPasswordController@showResetForm');
        // });
    });

   
    
   
    
        //  /** Set prefix for Tenant as clients with auth*/
        // Route::group(["middleware" => ["auth"]], function () {
        //     Route::get('/home', 'HomeController@index')->name('clienthome');
        //     Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
        // });
        
        // Route::get('/login', 'Auth\LoginController@showLoginForm')->name('clientlogin');
        // Route::post('/login', 'Auth\LoginController@login')->name('clientlogin');

    // Route::get('/', function () {
    //     //return view('welcome');
    //     return bcrypt("password");
    // });
    
    
    // Route::get('/home', 'HomeController@index')->name('home');
    // Route::get('/getapi', 'HomeController@getapi')->name('getapi');
    // Route::get('/task/{id}', 'HomeController@task')->name('task');

    
    
});






